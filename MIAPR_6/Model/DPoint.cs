﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hierarchical_grouping.Model
{
    class DPoint
    {
        public double X;
        public double Y;

        public DPoint(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
}
