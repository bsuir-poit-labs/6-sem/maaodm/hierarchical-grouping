﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hierarchical_grouping.Model
{
    class Distances
    {
        public readonly double Distance;
        public readonly Group Group;

        public Distances(double dist, Group sub)
        {
            Distance = dist;
            Group = sub;
        }
    }
}
