﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hierarchical_grouping.Model
{
    class Group
    {
        public List<Distances> Distances = new List<Distances>();
        public List<Group> SubGroups = new List<Group>();
        public string Name;
        public double X;
        public double Y;

        public double GetDistance(Group group)
        {
            foreach (Distances distance in Distances)
                if (distance.Group.Equals(group))
                    return distance.Distance;

            return -1;
        }

        public void DeleteDistances(List<Group> deleteList)
        {
            foreach (Group deleteGroup in deleteList)
            {
                var deleteDistances = new List<Distances>();

                foreach (Distances distance in Distances)
                    if (distance.Group.Equals(deleteGroup))
                        deleteDistances.Add(distance);
                foreach (Distances deleteDistance in deleteDistances)
                    Distances.Remove(deleteDistance);
            }
        }
    }
}
